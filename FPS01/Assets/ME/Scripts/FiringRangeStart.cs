﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FiringRangeStart : MonoBehaviour
{
    public GameObject FiringRangePanel;
    public GameObject TargetManager;
    public bool canStart = false;
    private bool countDown = false;

    public float countdownTimer = 5;
    private float countdownTime;
	
    // Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        StartTimer();
        if (canStart && Input.GetKey(KeyCode.E))
        {
            countDown = true;   
        }
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "MainCamera")
        {
            canStart = true;
            FiringRangePanel.SetActive(true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "MainCamera")
        {
            canStart = false;
            FiringRangePanel.SetActive(false);
        }
    }

    public void StartTimer()
    {
        TargetManager tMan = TargetManager.GetComponent<TargetManager>();
        if (countDown)
        {
            countdownTimer -= Time.deltaTime;
            if (countdownTimer <= countdownTime)
            {
                canStart = false;
                countDown = false;
                countdownTimer = 5;
                tMan.SpawnTarget();
            }
        }
    }
}
