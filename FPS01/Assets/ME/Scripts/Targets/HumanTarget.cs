﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanTarget : MonoBehaviour
{
    public GameObject targetManager;
    private TargetManager tMan;

    public float minAngle = 0.0f;
    public float maxAngle = 90.0f;
    public float rotSpeed = 1.0f;

    public bool isReady = true;
	// Use this for initialization
	void Start ()
    {
        Invoke("RiseLerp", 2);
        transform.eulerAngles = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update ()
    {
       tMan = targetManager.GetComponent<TargetManager>();
        if(!isReady)
        {
            tMan.SpawnTarget();
            FallLerp();
        }
    }

    public void RiseLerp()
    {
        float riseAngle = Mathf.LerpAngle(maxAngle, minAngle, rotSpeed);
        transform.eulerAngles = new Vector3(riseAngle, 0, 0);
    }

    public void FallLerp()
    {
        float fallAngle = Mathf.LerpAngle(minAngle,- maxAngle, rotSpeed);
        transform.eulerAngles = new Vector3(fallAngle, 0, 0);
        Destroy(gameObject);
      //  targetManager
    }

}