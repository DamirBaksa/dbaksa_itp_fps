﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text counterText;
    private UIManager UI;

    public float seconds, minutes;
    public bool gameOn = false;

    // Use this for initialization
    void Start()
    {
        UI = GetComponent<UIManager>();
        counterText = GetComponent<Text>() as Text;
    }

    // Update is called once per frame

    void Update()
    {
        if (gameOn)
        {
            gameTimer();
        }
    }

    public void LoseGame()
    {
        UI.GetComponent<UIManager>().levelLoad("Main Menu");
    }

    public void gameTimer()
    {
        minutes = (int)(Time.time / 60f);
        seconds = (int)(Time.time % 60f);
        counterText.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }
}
