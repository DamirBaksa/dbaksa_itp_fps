﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject currentWeapon;
    public GameObject weaponManager;

    private WeaponManager wMan;
    private WeaponBase wBase;

    public GameObject AmmoTextObj;
    [HideInInspector]
    public Text ammoText;
    // Use this for initialization
    void Start()
    {
        wMan = weaponManager.GetComponent<WeaponManager>();

    }
	// Update is called once per frame
	void Update ()
    {
       // currentWeapon = wMan.currentWeapon;   
        wBase = currentWeapon.GetComponent<WeaponBase>();
	}

    public void levelLoad(string LeveltoLoad)
    {
        // load scene 
        SceneManager.LoadScene(LeveltoLoad, LoadSceneMode.Single);
    }
}
