﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour
{
    public List<GameObject> hTarget = new List<GameObject>();

    private GameObject target;
    public GameObject targetPreFab;
    private HumanTarget humanTarget;

    private float xSpawnBoundary1 = 67;
    private float xSpawnBoundary2 = 128;
    private float zSpawnBoundary1 = 107;
    private float zSpawnBoundary2 = 170;
    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void SpawnTarget()
    {
        GameObject clone = CreateTarget();
    }

    public GameObject CreateTarget()
    {
        Vector3 startPos = new Vector3(Random.Range(xSpawnBoundary1, xSpawnBoundary2), 0.4f, Random.Range(zSpawnBoundary1, zSpawnBoundary2));

        GameObject target = Instantiate(targetPreFab, startPos, Quaternion.identity);

        return target;
    }
}
