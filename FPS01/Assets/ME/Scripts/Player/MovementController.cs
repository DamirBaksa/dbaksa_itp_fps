﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    private Transform _target;
    private Vector3 _direction;

    public float moveSpeed;

    // Accessors & mutators (aka getters and setters)
    // C++ common convention
    public void SetTarget(Transform target) { _target = target; }
    public Transform GetTarget() { return _target; }

    // C# property = makes for easy and implicit accessors & mutators
    // property utilizing a separate private variable
    public Transform target { get { return _target; } set { _target = value; } }
    // auto property 
    // - enables a public variable that is not exposed in the inspector
    // - enables very simple read-only variables
    public Transform example01 { get; set; }
    // readable by any class (with a reference) but modifiable only by this class
    public Transform example02 { get; private set; }



    public void SetDirection(Vector3 direction)
    {
        _target = null;
        _direction = direction;
    }

    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        _direction.Normalize();
        transform.Translate(_direction * moveSpeed * Time.deltaTime, Space.World);
	}
}
