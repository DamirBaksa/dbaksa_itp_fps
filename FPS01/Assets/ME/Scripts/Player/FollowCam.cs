﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowCam: MonoBehaviour
{
    private MovementController movementController;
    public Transform playerCamera;
    //private Quaternion 

    private float multiplier = 1;

	// Use this for initialization
	void Start ()
    {
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        movementController = GetComponent<MovementController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 tDir = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
        {
            tDir = transform.forward* multiplier;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            tDir = -transform.forward * multiplier;
        }
        if (Input.GetKey(KeyCode.D))
        {
            tDir += transform.right * multiplier;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            tDir += -transform.right * multiplier;
        }
        tDir.y = 0.0f;
        tDir.Normalize();
        movementController.SetDirection(tDir);

        Vector3 mouseDelta = new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0f);
        //mouseDelta.Normalize();

        transform.Rotate(0.0f, mouseDelta.y, 0.0f, Space.World);
        playerCamera.Rotate(mouseDelta.x, 0.0f, 0.0f, Space.Self);
    }



}
