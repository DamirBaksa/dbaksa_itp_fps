﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flash : MonoBehaviour
{
    private Text flashingText;
    void Start()
    {
        flashingText = GetComponent<Text>();
        
    }

    private void Update()
    {
        StartCoroutine(BlinkText());  //Calling flashingtext coroutine on start
    }
    //function to blink the text
    public IEnumerator BlinkText()
    {
        while (true)
        {
            //set the Text's text to blank
            flashingText.text = "";
            //display blank text for 0.5 seconds
            yield return new WaitForSeconds(.5f);
            //display “I AM FLASHING TEXT” for the next 0.5 seconds
            flashingText.text = "** RELOAD **";
            yield return new WaitForSeconds(.5f);
        }
    }
}


