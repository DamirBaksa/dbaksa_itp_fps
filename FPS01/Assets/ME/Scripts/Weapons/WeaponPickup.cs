﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class WeaponPickup : MonoBehaviour
{
    public Text pickupText;
    public GameObject Panel;

    public GameObject pistolToDrop;
    public GameObject m4ToDrop;
    public GameObject laserGuntoDrop;

    public GameObject pistol;
    public GameObject m4;
    public GameObject laserGun;
    public GameObject currentWeapon;

    [HideInInspector]
    public bool pistolPickUp = false;
    [HideInInspector]
    public bool m4PickUp = false;
    [HideInInspector]
    public bool laserGunPickUp = false;

    public enum Weapons 
    {
        Hands,
        Pistol, 
        M4,
        LaserGun,
    }
    public Weapons weapons;
	// Use this for initialization
	void Start ()
    {
        weapons = Weapons.Hands;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Pickup();
        SwitchWeapon();
    }
    
    public void SwitchWeapon()
    {
        switch (weapons)
        {
            case Weapons.Hands:

                break;

            case Weapons.Pistol:
                Debug.Log("Pistol picked up");
                Destroy(pistolToDrop);
                    if (currentWeapon !=null)
                    {
                        currentWeapon.SetActive(false);
                    }
                    pistol.SetActive(true);
                    currentWeapon = pistol;
                Panel.SetActive(false);
                break;

            case Weapons.M4:
                Debug.Log("M4 picked up");
                Destroy(m4ToDrop);
                    if (currentWeapon != null)
                    {
                        currentWeapon.SetActive(false);
                    }
                m4.SetActive(true);
                    currentWeapon = m4;
                Panel.SetActive(false);
                break;

            case Weapons.LaserGun:
                Debug.Log("LaserGun picked up");
                Destroy(laserGuntoDrop);
                if (currentWeapon != null)
                {
                    currentWeapon.SetActive(false);
                }
                laserGun.SetActive(true);
                    currentWeapon = laserGun;
                Panel.SetActive(false);
                break;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Pistol")
        {
            Debug.Log("Pistol");
            Panel.SetActive(true);
            pickupText.text = "Press F to pick up Pistol";
            pistolPickUp = true;
        }
        else if (other.gameObject.tag == "M4") 
        {
            Debug.Log("M4");
            Panel.SetActive(true);
            pickupText.text = "Press F to pick up M4";
            m4PickUp = true;
        }
        else if (other.gameObject.tag == "LaserGun")
        {
            Debug.Log("LaserGun");
            Panel.SetActive(true);
            pickupText.text = "Press F to pick up LaserGun";
            laserGunPickUp = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Pistol")
        {
            Panel.SetActive(false);
            pistolPickUp = false;
        }
        else if (other.gameObject.tag == "M4")
        {
            Panel.SetActive(false);
            m4PickUp = false;
        }
        else if (other.gameObject.tag == "LaserGun")
        {
            Panel.SetActive(false);
            laserGunPickUp = false;
        }
    }

    public void Pickup()
    {
        SwitchWeapon();
        if (pistolPickUp && Input.GetKey(KeyCode.F))
        {  
            weapons = Weapons.Pistol;
        }
        if (m4PickUp && Input.GetKey(KeyCode.F))
        {
            weapons = Weapons.M4;
        }
        if (laserGunPickUp && Input.GetKey(KeyCode.F))
        {
            weapons = Weapons.LaserGun;
  
        }

        
    }
}

