﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponBase : MonoBehaviour
{
    public enum GunType { Pistol, AR, Shotgun, Launcher }
    public GunType gun;

    public Camera fpsCam;
    public ParticleSystem muzzleFlash;
    public Animator anim;
    public GameObject  reloadText, impactEffect;
    public Text gunModeText;
    private Text ammoText;
    private WeaponManager weaponManager;


    [Header("Shooting Variables")]
    [Range(10, 100)]
    public float damage = 10;
    [Range(0, 800)]
    public float range = 100;
    public float fireRate = 15;
    public bool canShoot = true;
    private float nextTimeToFire = 0;
    private bool shootKey; 
    
    [Header("Ammo/Reload Variables")]
    public int maxAmmo = 10;
    public float reloadTime = 1;
    private int currentAmmo;
    private bool isReloading = false;
    private bool autoMode = false;

    [Header("ShotgunSpread Variables")]
    public float spreadAngle;
    public GameObject PelletPF;
    public Transform shotSpot;
    private int pelletCount;
    List<Quaternion> pellets;

    [Header("GrenadeLauncher Variables")]
    public GameObject Grenade;
    public float launchForce = 100;

    // Use this for initialization
    void Start ()
    {
        if (gameObject.tag == "Pistol")
        {
            gun = GunType.Pistol;
        }
        else if (gameObject.tag == "AR")
        {
            gun = GunType.AR;
        }
        else if (gameObject.tag == "Shotgun")
        {
            gun = GunType.Shotgun;
        }
        else if (gameObject.tag == "Launcher")
        {
            gun = GunType.Launcher;
        }

        switch (gun)
        {
            case GunType.Pistol:
                maxAmmo = 10;
                reloadTime = 0.5f;

                break;

            case GunType.AR:
                maxAmmo = 30;

                break;

            case GunType.Shotgun:
                maxAmmo = 8;
                pelletCount = 10;
                pellets = new List<Quaternion>(new Quaternion[pelletCount]);

                break;

            case GunType.Launcher:
                maxAmmo = 4;
                reloadTime = 2;

                break;
        }
        weaponManager = GetComponentInParent<WeaponManager>();
        currentAmmo = maxAmmo;	
	}
	
	// Update is called once per frame
	void Update ()
    {
        shootKey = Input.GetButtonDown("Fire1");
        if (isReloading)
        {
            return;
        }
        if (currentAmmo <= 0)
        {
            currentAmmo = 0;
            reloadText.SetActive(true);
            if (Input.GetKeyDown(KeyCode.R))
            {
                StartCoroutine(Reload());
                return;
            }
        }
        else if (currentAmmo > 0 && currentAmmo < maxAmmo)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                StartCoroutine(Reload());
                return;
            }
        }
        else reloadText.SetActive(false);
        Text text = weaponManager.ammoText;
        text.text = currentAmmo + " / " + maxAmmo;

        if (Input.GetButton("Fire2"))
        {
            Zoom();
        }
        if (Input.GetButtonUp("Fire2"))
        {
            anim.SetBool("Aiming", false);
        }

        switch (gun)
        {
            case GunType.Pistol:
                maxAmmo = 10;
                reloadTime = 0.5f;
                RayCastShoot();
       
                break;

            case GunType.AR:
                maxAmmo = 30;
                SemiOrAuto();
                RayCastShoot();
                
                break;

            case GunType.Shotgun:
                maxAmmo = 8;
                pelletCount = 10;
                ShotgunSpread();
                break;

            case GunType.Launcher:
                maxAmmo = 4;
                LaunchGrenade();
                break;
        }
    }

    public void Zoom()
    {
        anim.SetBool("Aiming", true);

        Debug.Log("Shot");
    }

    #region Shooting
    public void RayCastShoot()
    {
        if (canShoot)
        {
            if (shootKey && Time.time >= nextTimeToFire && currentAmmo != 0)
            {
                muzzleFlash.Play();
                nextTimeToFire = Time.time + 1 / fireRate;
                RaycastHit hit;
                if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
                {
                    Debug.Log(hit.transform.name);
                    GameObject impactGo = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
                    Destroy(impactGo, 2);

                    if (hit.collider.gameObject.tag == "HumanTarget")
                    {
                        GameObject targ = hit.collider.gameObject;
                        HumanTarget hTarget = targ.GetComponent<HumanTarget>();
                        hTarget.isReady = false;
                    }
                }
                RecoilCall();
                currentAmmo--;
            }
        }
    }
    public void ShotgunSpread()
    {
        if (canShoot)
        {
            if (shootKey && Time.time >= nextTimeToFire && currentAmmo != 0)
            {
                muzzleFlash.Play();
                currentAmmo--;
                StartCoroutine(ShotgunPump());
                int i = 0;
                foreach (Quaternion quat in pellets)
                {
                    pellets[i] = Random.rotation;
                    GameObject pellet = Instantiate(PelletPF, shotSpot.position, shotSpot.rotation);
                    i++;
                    pellet.transform.rotation = Quaternion.RotateTowards(pellet.transform.rotation, pellets[i], spreadAngle);
                    Debug.Log("SHOTshot");
                }
            }
        }
    }

    public void LaunchGrenade()
    {
        if (canShoot)
        {
            if (shootKey && Time.time >= nextTimeToFire && currentAmmo != 0)
            {
                currentAmmo--;
                GameObject Gren = Instantiate(Grenade, shotSpot.position, shotSpot.rotation);
                Rigidbody rb = Gren.GetComponent<Rigidbody>();
                rb.AddForce(shotSpot.forward * launchForce);
                RecoilCall();
            }
        }
    }
    #endregion

    #region Reload and gunMode
    public IEnumerator Reload()
    {
        anim.SetBool("Reloading", true);
        isReloading = true;
        Debug.Log("Reloading");

        yield return new WaitForSeconds(reloadTime);

        anim.SetBool("Reloading", false);

        currentAmmo = maxAmmo;
        isReloading = false;
    }

    public IEnumerator ShotgunPump()
    {
        var pumpTime = 0.6f;

        canShoot = false;
        anim.SetBool("Pump", true);

        yield return new WaitForSeconds(pumpTime);

        anim.SetBool("Pump", false);
        canShoot = true;
    }

    public void SemiOrAuto()
    {
        if (Input.GetKey(KeyCode.Q) && !autoMode)
        {
            autoMode = true;
            
        }
        else if (Input.GetKey(KeyCode.Q) && autoMode)
        {
            
            autoMode = false;
        }
            if (autoMode)
            {
                gunModeText.text = "AUTO";
                shootKey = Input.GetButton("Fire1");
            }
            else
            {
                gunModeText.text = "SEMI";
                shootKey = Input.GetButtonDown("Fire1");
            }
    }
    #endregion

    #region Recoil(s)
    public void RecoilCall()
    {
        switch (gun)
        {
            case GunType.Pistol:
                anim.enabled = false;
                StartCoroutine(PistolRecoil());

                break;

            case GunType.AR:

             //   StartCoroutine(ARRecoil());

                break;

            case GunType.Launcher:

                StartCoroutine(GLRecoil());

                break;
        }
    }

    public IEnumerator PistolRecoil()
    {
        canShoot = false;
        var recoilTime = 0.2f;
        float regAngle = 0.0f;
        float endAngle = -60.0f;

        float recAngle = Mathf.Lerp(regAngle, endAngle, Time.time);
        float retAngle = Mathf.Lerp(endAngle, regAngle, Time.time);
        transform.localEulerAngles = new Vector3(recAngle, 0, 0);

        yield return new WaitForSeconds(recoilTime);

        transform.localEulerAngles = new Vector3(retAngle, 0, 0);
        canShoot = true;
        anim.enabled = true;
    }

    public IEnumerator GLRecoil()
    {
        var recoilTime = 0.2f;
        canShoot = false;
        anim.SetBool("Recoiling", true);

        yield return new WaitForSeconds(recoilTime);

        anim.SetBool("Recoiling", false);
        canShoot = true;
    }

    public void ARRecoil()
    {

    }
    #endregion

}
