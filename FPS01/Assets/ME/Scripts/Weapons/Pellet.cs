﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pellet : MonoBehaviour {

    public GameObject shotgun;
    public float pelletSpeed;
    private float distance;
	// Use this for initialization
	void Start ()
    {
        

    }
	
	// Update is called once per frame
	void Update ()
    {
        distance = Vector3.Distance(shotgun.transform.position, transform.position);
        transform.Translate(Vector3.forward * pelletSpeed);
        WeaponBase shotgunBase = shotgun.GetComponent<WeaponBase>();
        if (distance >= shotgunBase.range) 
        {
            Destroy(gameObject);
        }
    }
}
