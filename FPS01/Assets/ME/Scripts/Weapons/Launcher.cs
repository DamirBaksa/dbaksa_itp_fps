﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    public Transform shotSpot;
    public GameObject Grenade;
    public float launchForce = 100;
    public float zoomSpeed = 10;

    public Vector3 zoomSight;
    public Vector3 hipFire;


    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (Input.GetButtonDown("Fire1"))
        {
            LaunchGrenade();
        }
        if (Input.GetButton("Fire2"))
        {
            Zoom();
        }
        if (Input.GetButtonUp("Fire2"))
        {
            HipFire();
        }
    }

    public void LaunchGrenade()
    {
        GameObject Gren = Instantiate(Grenade, shotSpot.position, shotSpot.rotation);
        Rigidbody rb = Gren.GetComponent<Rigidbody>();
        rb.AddForce(shotSpot.forward * launchForce);
    }

    public void Zoom()
    {
        transform.localPosition = Vector3.Slerp(transform.localPosition, zoomSight, zoomSpeed * Time.deltaTime);
    }

    public void HipFire()
    {
        transform.localPosition = hipFire;
    }
}
