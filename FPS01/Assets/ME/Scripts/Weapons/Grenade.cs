﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    public GameObject explosionPF;
    public float delay = 3;
    public float explosionRadius = 10;
    public float explosionForce = 500;
    private float timer;
    private bool hasExploded = false;
	// Use this for initialization
	void Start ()
    {
        timer = delay;	
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer -= Time.deltaTime;	
        if (timer <= 0 && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
	}
    public void Explode()
    {
        Instantiate(explosionPF, transform.position, transform.rotation);
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider Obj in colliders)
        {
            Rigidbody rb = Obj.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);
            }
        }  
        Destroy(gameObject);
    }
}
