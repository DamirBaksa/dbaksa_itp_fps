﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickupTest : MonoBehaviour
{

    public GameObject weaponManager;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        WeaponBase weapon = other.GetComponent<WeaponBase>();
        if(weapon != null)
        {
            other.transform.parent = weaponManager.transform;
        }
    }
}
